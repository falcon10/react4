import React, {createContext, useContext, useState} from 'react';

export const CharStats = createContext ({
  name: "Demon",
  str: 0,
  hp:0,
  speed: 10,
  dmg: 0,
  lvl: 1,
  addStr: item => {},
  addName: item => {}
})

export const GameState = createContext ({
  isName: false,
  setName: item => {}
})

export const GameApp = ({children}) => {
  const [isName, setName] = useState(false);

  return(
    <GameState.Provider
      value = {{
        isName,
        setName
      }}
    >
     {children}
    </GameState.Provider>
  )
}

export const UserStatsProvider = ({children}) => {
  const [name, addName] = useState('');
  const [str, addStr] = useState(10);
  const hp = 12;
  const speed = 2;
  const dmg = speed*str;
  const lvl = 1;

  return (
    <CharStats.Provider
      value = {{
        name,
        str,
        hp,
        speed,
        addName,
        addStr,
        dmg,
        lvl
      }}
      >
      {children}
      </CharStats.Provider>
  )
}

export const OponentStatProvider = ({children}) => {
  const name = 'Oponent';
  const str = 10;
  const hp = 10;
  const speed = 2;
  const dmg = speed*str;
  const lvl = 1;

  return (
    <CharStats.Provider
      value = {{
        name,
        str,
        speed,
        hp,
        dmg,
        lvl
      }}
      >
       {children}
    </CharStats.Provider>
  )
}
