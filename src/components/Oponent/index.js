import React, { useContext } from 'react';
import {CharStats} from '../../context';
import TableRow from '../TableRow'

const Oponent = props => {
  const {name, str, hp, speed, dmg, lvl} = useContext(CharStats);
  const myStats = {name, str, hp, speed, dmg, lvl};

    return (
      <div className="col-6">
      {props.alive &&
     <TableRow {...myStats} />   }
     </div>
    )
}

export default Oponent;
