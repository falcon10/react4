import Style from './style.css';
import {useContext, useEffect} from 'react';
import {CharStats} from '../../context'
import TableRow from "../TableRow";
import ProgressBar from '../../components/ProgressBar'



const User = () => {
  const { name, str, hp, speed, addStr, addName, dmg, lvl } = useContext(CharStats);
  const myStats = {name, str, hp, speed, dmg, lvl};
  useEffect(()=>{addName(localStorage.getItem('name'))},[])
  return (
    <div className="col-6">
     <TableRow {...myStats} />
     <ProgressBar />
    </div>
  )
}

export default User;
