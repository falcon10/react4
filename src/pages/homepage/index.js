import React,{useContext, useState, useEffect} from 'react';
import Form from '../../components/Form';
import User from '../../components/User';
import Oponent from '../../components/Oponent';
import {
  UserStatsProvider,
  OponentStatProvider,
  CharStats,
  GameApp,
  GameState,
} from '../../context';


const Homepage = () => {
  const {name, str, hp, speed, addStr} = useContext(CharStats);
  const {isName, setName} = useContext(GameState);

  useEffect (()=>{
  console.log(isName)
  // const getName = localStorage.getItem('name');
  // if (getName.length > 0) (setName(true))
},[isName])

return (
  <div>
    {isName ? (
      <div className="game__wrapper">
        <UserStatsProvider>
          <User />
        </UserStatsProvider>

        <OponentStatProvider>
          <Oponent alive />
        </OponentStatProvider>
      </div>
    ) : (
      <Form />
    )}
  </div>
 );
};

export default Homepage;
